from django.urls import re_path
from . import views

app_name = 'book'

#url for app
urlpatterns = [
    re_path(r'^$', views.form, name='form'),
    re_path(r'^delete-activity/(?P<pk>\d+)/$', views.delete_activity, name='delete_activity'),
]
