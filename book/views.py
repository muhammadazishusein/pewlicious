from django.shortcuts import render
from django.shortcuts import render
from .models import ActivityModel
from .forms import ActivityForm
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages


# Create your views here.

def form(request):
    if request.method == "POST":
        data = ActivityForm(request.POST)
        if data.is_valid():
            data.save()
        data = ActivityModel.objects.all()

    if request.method == "GET":
        data = ActivityModel.objects.all()

    return render(request, 'form.html', {'form': ActivityForm(), 'data': data})

def delete_activity(request, pk):
    activity = get_object_or_404(ActivityModel, pk=pk)
    activity.delete()
    
    form = ActivityForm(instance=activity)

    context = {
        'form' : form,
    }
    return render (request, 'form.html', context)
