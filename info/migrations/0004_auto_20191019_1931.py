# Generated by Django 2.2.5 on 2019-10-19 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0003_auto_20191019_1930'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='image',
            field=models.BinaryField(default=''),
        ),
    ]
