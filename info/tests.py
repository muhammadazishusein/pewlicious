from django.test import TestCase
from django.urls import resolve

from .views import info
from homepage.models import Restaurant

from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

# Create your tests here.

class UnitTest(TestCase):
    def test_found_view(self):
        found = resolve('/info/')
        self.assertEqual(found.func, info)
    
    def test_aja(self):
        self.assertEqual(True, not False)

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()
    
    def test_functional(self):
        self.browser.get('http://localhost:8000/info/?arg=1')

        #Test Judul
        self.assertIn('Info Restoran', self.browser.title)

        #Test Tulisan
        tulisan = self.browser.find_element_by_id('not-login').text
        self.assertIn('login', tulisan)

        self.browser.quit()


if __name__ == '__main__':
    unittest.main(warnings='ignore')