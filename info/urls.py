from django.urls import path, include
from . import views
from django.conf.urls import url, include

app_name = 'info'

urlpatterns = [
    path('', views.info, name = "info"),
    path('addLikes/', views.addLikes, name = "addLikes")
]