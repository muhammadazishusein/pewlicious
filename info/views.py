from django.shortcuts import render, redirect
from .forms import ReviewForm
from .models import Review
from homepage.models import Restaurant
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.

def info(request):
    if request.method == 'GET':
        form = ReviewForm()
    elif request.method == 'POST':
        form = ReviewForm(request.POST)
        # print(request.POST)

        if form.is_valid():
            new_review = Review(
                namaResto = Restaurant.objects.get(id = int(request.GET.get('arg', ''))),
                name = form.data['name'],
                content = form.data['content'],
            )

            num = request.GET.get('arg', '')
            new_review.save()
            form = ReviewForm()

        return HttpResponseRedirect('/info/?arg=' + num)
    

    resto = Restaurant.objects.get(id = int(request.GET.get('arg', '')))
    reviews = Review.objects.filter(namaResto = resto)


    context = {
        'form': form,
        'reviews': reviews,
        'resto': resto,
    }

    return render(request, 'info.html', context)

def addLikes(request):
    resto = Restaurant.objects.get(id = int(request.GET.get('arg', '')))
    resto.likes += 1
    resto.save()
    return HttpResponse(content=resto.likes)