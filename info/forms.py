from django import forms
from .models import Review
from django.forms import ModelForm, Textarea
# import datetime

class ReviewForm(ModelForm):
    class Meta:
        model = Review

        fields = ["name", 'content']

        widgets = {
            'content': Textarea(attrs={'cols': 80, 'rows': 5}),
        }