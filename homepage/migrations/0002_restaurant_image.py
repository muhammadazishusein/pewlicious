# Generated by Django 2.2.5 on 2019-10-19 12:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='image',
            field=models.FileField(null=True, upload_to='images/', verbose_name=''),
        ),
    ]
