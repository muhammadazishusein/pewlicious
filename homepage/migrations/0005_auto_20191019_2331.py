# Generated by Django 2.2.5 on 2019-10-19 16:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0004_auto_20191019_2316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='image',
            field=models.FileField(null=True, upload_to='static/', verbose_name=''),
        ),
    ]
