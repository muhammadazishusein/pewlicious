from django import forms
from .models import Rest, Restaurant
from django.forms import ModelForm
from homepage import models

class RestForm(forms.Form):

    place   = forms.CharField(
            label       = 'tempat',
            max_length  = 50,
            required    = True,
            widget      = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'gabut'})    
        )

class RestaurantForm(forms.Form):

    nama    = forms.CharField(
            label       = 'tempat',
            max_length  = 50,
            required    = True,
            widget      = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'gabut'})    
        )
