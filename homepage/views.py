from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect

from .models import Rest
from .forms import RestForm

# Create your views here.
def home(request):
    html    = "homepage.html"
    if request.method == 'GET':
            form = RestForm()
    elif request.method == 'POST':
            form = RestForm(request.POST)
            #print(request.POST)
            if form.is_valid():
                new_rest = Rest(
                    place= form.data['place']
                )

                new_rest.save()
                print(Rest.objects.all())
                form = RestForm()

                return redirect('rest')
            print(form.errors)
            
    rest    = Rest.objects.all()
    context = {
            'form': form,
            'Rest': rest,
    }
    return render(request, html, context)

from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
