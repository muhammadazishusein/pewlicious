from django.db import models
from django.utils import timezone

# from django.forms import ModelForm

# Create your models here.
   
class Rest(models.Model):
    
    place   = models.CharField(max_length= 30 ,default="jabodetabek")

    def __str__(self):
        return self.place

class Restaurant(models.Model):
    places  = models.ForeignKey(Rest, on_delete=models.CASCADE)
    nama    = models.CharField(max_length = 30 , default="Random")
    alamat = models.CharField(max_length = 50, default = 'Jl. Depok Kota Depok')
    phone = models.CharField(max_length = 30, default = '0878xxxx')
    desc = models.TextField(default = '')


    likes = models.IntegerField(default = 0)
    image = models.CharField(max_length = 100, default = ".")

    def __str__(self):
        return self.nama
