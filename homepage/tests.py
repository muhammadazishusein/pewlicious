from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
# Create your tests here.

from .views import home
from .forms import RestForm

class iniTest(TestCase):
    def test_url_exists(self):
        response    = self.client.get('/');
        self.assertEqual(response.status_code, 200);

    def test_ini_htmlnya_bener_ga(self):
        response    = self.client.get('/');
        self.assertTemplateUsed(response, 'homepage.html');

    def test_ini_base_htmlnya_bener_ga(self):
        response    = self.client.get('/');
        self.assertTemplateUsed(response, 'homepagebase.html');

    def test_ini_ngapain(self):
        self.assertEqual(True,not False);

    def test_viewnya_bener_kaga(self):
        found       = resolve('/');
        self.assertEqual(found.func, home);

    def test_ini_beneran_html_kan(self):
        responseget = Client().get('/');
        html        = responseget.content.decode();
        self.assertTrue(html.startswith('<!DOCTYPE html>'));
        self.assertIn('<html>', html);
        self.assertTrue(html.endswith('</html>'));

    def test_login_Ga(self):
        responseget = Client().get('/');
        self.assertIn('Login', responseget.content.decode());
