from django.contrib import admin

# Register your models here.
from .models import Rest, Restaurant
admin.site.register(Rest)
admin.site.register(Restaurant)
