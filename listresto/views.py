from django.shortcuts import render
from homepage.models import Restaurant

# Create your views here.
def listresto(request):
    restos = Restaurant.objects.all()

    context = {
        'restos': restos,
    }

    return render(request, 'listresto.html', context)