from django.test import TestCase, Client
from django.utils import timezone
from django.urls import resolve
from listresto.views import listresto
from django.http import HttpRequest

# Create your tests here.
class UnitTest(TestCase):
    def test_ada_url(self):
        response = Client().get('/listresto/')
        self.assertEqual(response.status_code, 200)

    def test_tidak_ada_url(self):
        response = Client().get('/listresto/hello')
        self.assertEqual(response.status_code, 404)

    def test_pake_func_views(self):
        found = resolve('/listresto/')
        self.assertEqual(found.func, listresto)

    def test_pake_base(self):
        response = Client().get('/listresto/')
        self.assertTemplateUsed(response, 'listresto.html')

# from django.test import SimpleTestCase, TestCase, Client
# from django.urls import reverse, resolve
# from django.core.files.uploadedfile import SimpleUploadedFile
# from .views import listresto
# from .models import *

# class Test(TestCase):
#     def setUp(self):
#         self.client = Client()
#         self.listresto_url = reverse('listresto:listresto')
#     def test_text_exist(self):
#         response = self.client.get(self.listresto_url)
#         self.assertIn('listresto', response.content.decode())

# class TestUrls(SimpleTestCase):
#     def test_url_is_resolved(self):
#         url = reverse('listresto:listresto')
#         self.assertEquals(resolve(url).func, listresto)

# class ViewTest(TestCase):
#     def setUp(self):
#         self.client = Client()
#         self.listresto_url = reverse('listresto:listresto')

# class UnitTest(TestCase):
#     def url_is_exist(self):
#         response = Client().get('/listresto/')
#         self.assertEqual(response.status_code, 200)
#     def url_doesnt_exist(self):
#         response = Client().get('/wek')
#         self.assertEqual(response.status_code, 404)
#     def test_home_using_index_function(self):
#         found = resolve('/listresto/')
#         self.assertEqual(found.func, views.listresto)
#     def test_template_used(self):
#         response = Client().get('/listresto/')
#         self.assertTemplateUsed(response, 'listresto.html')