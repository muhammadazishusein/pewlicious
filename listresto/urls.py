from django.urls import path, include
from . import views
from django.conf.urls import url, include

app_name = 'listresto'

urlpatterns = [
    path('', views.listresto, name = "listresto"),
]